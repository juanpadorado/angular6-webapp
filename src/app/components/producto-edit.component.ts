import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../Models/producto';
import { GLOBAL } from '../services/global';

@Component({
    selector: 'producto-edit',
    templateUrl: '../views/producto-add.html',
    providers: [ProductoService]
})

export class ProductoEditComponent implements OnInit {

    public producto: Producto;
    public titulo: string;
    public filesToUpload;
    public is_edit;

    constructor(
        private _productoService: ProductoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Editar producto';
        this.producto = new Producto(0,'','',0,'');
        this.is_edit = true;
    }

    ngOnInit() { 
        this.getProducto();
    }

    onSubmit(){
        
        if(typeof(this.filesToUpload) !== 'undefined'){
            this._productoService.makeFileRequest(GLOBAL.url+'upload-file', [], this.filesToUpload).then((result:any) =>{
                    console.log(result);
                    this.producto.imagen = result.filename;
                    this.updateProducto();

                }, (error) =>{
                    console.log(error);
                }
            );
        }else{
            this.updateProducto();
        }
    }

    updateProducto(){
        this._route.params.forEach((params: Params) => {
            let id = params['id'];
            this._productoService.editProducto(id, this.producto).subscribe(
                (response:any) => {
                    if(response.code == 200){
                        this._router.navigate(['/producto', id]);
                    }else{
                        console.log(response);
                    }
                },
                error => {
                    console.log(<any>error);
                }
            );
        });
    }

    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>>fileInput.target.files;
        console.log(this.filesToUpload);
    }

    getProducto(){
        this._route.params.forEach((params: Params) => {
            let id = params['id'];

            this._productoService.getProducto(id).subscribe(
                (response:any) =>{
                    if(response.code == 200){
                        this.producto = response.data;
                    }else{
                        this._router.navigate(['/productos']);
                    }
                }, error => {
                    console.log(<any>error);
                }
            );
        });
    }

}