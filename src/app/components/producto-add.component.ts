import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../Models/producto';
import { GLOBAL } from '../services/global';

@Component({
    selector: 'producto-add',
    templateUrl: '../views/producto-add.html',
    providers: [ProductoService]
})

export class ProductoAddComponent implements OnInit {
    public titulo: string;
    public producto : Producto;
    public filesToUpload;
    public is_edit;

    constructor(
        private _productoService: ProductoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) { 
        this.titulo = 'Crear un nuevo producto';
        this.producto = new Producto(0, '', '', 0, '');
        this.is_edit = false;
    }

    ngOnInit() { 

    }

    onSubmit(){
        
        if(typeof(this.filesToUpload) !== 'undefined'){
            this._productoService.makeFileRequest(GLOBAL.url+'upload-file', [], this.filesToUpload).then((result:any) =>{
                    console.log(result);
                    this.producto.imagen = result.filename;
                    this.saveProducto();

                }, (error) =>{
                    console.log(error);
                }
            );
        }else{
            this.saveProducto();
        }
    }

    saveProducto(){
        this._productoService.addProducto(this.producto).subscribe(
            (response:any) => {
                if(response.code == 200){
                    this._router.navigate(['/productos']);
                }else{
                    console.log(response);
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>>fileInput.target.files;
        console.log(this.filesToUpload);
    }

}